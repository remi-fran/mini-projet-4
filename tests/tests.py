import unittest
from process import genererTerrain
from process import calculEtatSuivant
from process import calculEtatSuivantCell
from process import nbCellFeuVoisine

class TestAutomateFunctions(unittest.TestCase):
    def test_genererTerrain(self):
        terrain = genererTerrain(100, 100, 1)
        self.assertEqual(len(terrain), 100)
        for ligne in range(100):
            self.assertEqual(len(terrain[ligne]), 100)
            for colonne in range(100):
                self.assertEqual(terrain[ligne], "arbre")

        terrain = genererTerrain(100, 100, 0)
        for ligne in range(100):
            self.assertEqual(terrain[ligne][colonne], "vide")

        #Valeur par défaut : ligne = 5, colonne = 5, taux = 0.4
        terrain = genererTerrain()
        compteurArbres = 0
        self.assertEqual(len(terrain), 5)
        for ligne in range(5):
            self.assertEqual(len(terrain[ligne]), 5)
            for colonne in range(5):
                if terrain[ligne][colonne] == "arbre":
                    compteurArbres += 1
        self.assertTrue(compteurArbre > 30 & compteurArbre < 60)

    def test_calculEtatSuivantCell(self):
        terrain == [["vide", "vide", "vide"], ["vide", "vide", "vide"], ["vide", "vide", "vide"]]
        self.assertEqual("vide", calculEtatSuivantCell(terrain, 3, 3, 1, 1))

        #test du passage de "feu" à "cendre"
        terrain == [["vide", "vide", "vide"], ["vide", "feu", "vide"], ["vide", "vide", "vide"]]
        self.assertEqual("cendre", calculEtatSuivantCell(terrain, 3, 3, 1, 1))

        #test du passage de "cendre" à "vide"
        terrain == [["vide", "vide", "vide"], ["vide", "cendre", "vide"], ["vide", "vide", "vide"]]
        self.assertEqual("vide", calculEtatSuivantCell(terrain, 3, 3, 1, 1))

        #test du passage de "arbre" à "arbre" ou "feu"
        terrain == [["vide", "vide", "vide"], ["vide", "arbre", "vide"], ["vide", "vide", "vide"]]
        self.assertEqual("arbre", calculEtatSuivantCell(terrain, 3, 3, 1, 1))
        terrain == [["vide", "feu", "vide"], ["vide", "arbre", "vide"], ["vide", "vide", "vide"]]
        self.assertEqual("feu", calculEtatSuivantCell(terrain, 3, 3, 1, 1))
        terrain == [["vide", "vide", "vide"], ["feu", "arbre", "vide"], ["vide", "vide", "vide"]]
        self.assertEqual("feu", calculEtatSuivantCell(terrain, 3, 3, 1, 1))
        terrain == [["vide", "vide", "vide"], ["vide", "arbre", "vide"], ["vide", "feu", "vide"]]
        self.assertEqual("feu", calculEtatSuivantCell(terrain, 3, 3, 1, 1))
        terrain == [["vide", "vide", "vide"], ["vide", "arbre", "feu"], ["vide", "vide", "vide"]]
        self.assertEqual("feu", calculEtatSuivantCell(terrain, 3, 3, 1, 1))
        terrain == [["vide", "feu", "vide"], ["feu", "arbre", "feu"], ["vide", "feu", "vide"]]
        self.assertEqual("feu", calculEtatSuivantCell(terrain, 3, 3, 1, 1))

    def test_calculEtatSuivant(self):
        terrain = [["vide", "arbre", "cendre"],["arbre", "feu", "arbre"],["arbre", "arbre", "vide"]]
        expected_result = [["vide", "feu", "vide"],["feu", "cendre", "feu"], ["arbre", "feu", "vide"]]
        self.assertEqual(expected_result, calculEtatSuivant(terrain, 3, 3))

    def test_nbCellFeuVoisine(self):
        terrain == [["vide", "vide", "vide"], ["vide", "arbre", "vide"], ["vide", "vide", "vide"]]
        self.assertEqual(0, nbCellFeuVoisine(terrain, 3, 3, 1, 1))
        terrain == [["vide", "feu", "vide"], ["vide", "arbre", "vide"], ["vide", "vide", "vide"]]
        self.assertEqual(1, nbCellFeuVoisine(terrain, 3, 3, 1, 1))
        terrain == [["vide", "feu", "vide"], ["feu", "arbre", "vide"], ["vide", "vide", "vide"]]
        self.assertEqual(2, nbCellFeuVoisine(terrain, 3, 3, 1, 1))
        terrain == [["vide", "feu", "vide"], ["vide", "arbre", "feu"], ["vide", "vide", "vide"]]
        self.assertEqual(3, nbCellFeuVoisine(terrain, 3, 3, 1, 1))
        terrain == [["vide", "feu", "vide"], ["feu", "arbre", "feu"], ["vide", "feu", "vide"]]
        self.assertEqual(4, nbCellFeuVoisine(terrain, 3, 3, 1, 1))